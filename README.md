# Summary

The present example demonstrates the use of high-performance computing (HPC) to estimate the uncertainty in the waveguide based measurement of dielectric constant (DC). The measurement model comprises a rectangular waveguide containing a dielectric object with a DC to be retrieved from the measured scattering parameters (S11 and S21). The target audience of this demo case are companies and institutions working in the area of material characterization, as well as manufacturers of dielectric resonators [1].

The ability to measure the DC with high accuracy is of particular importance in microwave technologies. Therefore, accurate measurement uncertainty estimation is required to check whether for a given configuration at a given frequency the DC value can be measured with an acceptable accuracy. 

In the demo the measurement uncertainty is estimated using the Monte Carlo method [2] whose accuracy depends solely on the number of trials that makes the estimation process very time-consuming. This issue motivated the authors of the demo to develop a program for the Monte Carlo method that leverages the power of parallel processing in order to appreciably reduce the time taken by the estimation process.

The demo case also includes the codes implementing algorithms for solving the forward scattering problem for a centered [3] and offset rod [4], as well as one- and two- dimensional minimum finding algorithms [5]. The minimum finding algorithms are necessary for solving the corresponding inverse problem - retrieve the DC value from the measured scattering data.

Additionally, the demo codes can be readily extended to handle multi-layered rods, as well. Such structures can be used in practice in measurements of the DC of liquid materials and powders. For example, a cylindrical container with thin dielectric walls filled with a liquid or powder to be measured can be regarded as a two layered rod: the inner layer being the material under test (MUT), while the outer one is the wall of the container. 

# Table of contents

[[_TOC_]]


# Problem background

The structure under consideration is composed of a rectangular waveguide section containing a dielectric cylindrical rod (cylindrical obstacle) as illustrated in Figures 1 and 2. The radius of the rod is denoted by $`r`$, $`a`$ is the width of the broader wall of the waveguide and $`x_{\mathrm{o}}`$ is the offset of the rod axis measured from the line of symmetry. Both the waveguide and rod have the same height and the structure is excited by a dominant waveguide mode. The reflection and transmission coefficients are denoted by $`S_{11}`$ and $`S_{21}`$, respectively. 

Such a configuration can be used to measure the dielectric constant of the rod, $`\varepsilon_{\mathrm{r}}`$ [1,2]. In the demo case is extracted from the measured $`|S_{11}|`$ through minimizing the objective function of $`Q(\varepsilon_{\mathrm{r}})=(|S_{11}|(\varepsilon_{\mathrm{r}})-|S_{11}|_{\mathrm{meas}})^{2}`$ the form: where $`|S_{11}|_{\mathrm{meas}}`$ is the measured absolute value of $`S_{11}`$ The minimum of is found using an iterative algorithm requiring 100-1000 iterations with a few evaluations of the scattering data per iteration. In fact, the algorithm calculates $`Q(\varepsilon_{\mathrm{r}})`$  at different until the value is found that gives  that is equivalent to $`|S_{11}|=|S_{11}|_{\mathrm{meas}}`$. In the demo the scattering data is computed with the aid of fast semi-analytical methods that considerably outperform the general-purpose methods in terms of the CPU time [3,4]. 

In practice the measured $`|S_{11}|`$ is always contaminated by some measurement noise due to numerous imperfections of measurement devices. This leads to a retrieved $`\varepsilon_{\mathrm{r}}`$ that differs from the true value - measurement uncertainty. Furthermore, values of other model parameters are also obtained via measurements, e.g. the radius of the rod is measured by means of digital calliper and therefore also contribute to the uncertainty of $`\varepsilon_{\mathrm{r}}`$. Many compact microwave devices are highly sensitive to small variations in $`\varepsilon_{\mathrm{r}}`$. In such a case even a small deviation of $`\varepsilon_{\mathrm{r}}`$ from the one assumed in the design process due to measurement uncertainty, $`\mathrm{u}(\varepsilon_{\mathrm{r}})`$, may lead to a dramatic deterioration of device performance. To mitigate this issue accurate estimation of $`\mathrm{u}(\varepsilon_{\mathrm{r}})`$ is necessary.

For measurement uncertainty estimation the Monte Carlo method is used. Although the method is highly reliable in estimating the measurement uncertainty, it is very computationally demanding. Fortunately, such problems can be effectively addressed by means of parallel computing. Owing to the fact that trails are not interrelated, the initial workload can be readily divided into a number of parallel processes that leads to a considerable reduction in the simulation time thereby making the estimation more practical. 

<img src="./README_img/MC_fig1.png" alt="Figure 1" width="400"/>

Figure 1: The geometry of the measurement model under study.

<img src="./README_img/MC_fig2.png" alt="Figure 2" width="400"/>

Figure 2: A shematic view of the structure being modelled


# Contents of this case

The demo case contains files and folders for the calculation of the scattering data, solving the inverse scattering problem (objective function minimization) and Monte Carlo based measurement uncertainty estimation. For the sake of user’s convenience, the model parameters, minimum finding function parameters and estimation parameters are stored a separate file. 
The files in the repository are arranged as follows:
  * `parameters.dat`    – the file containing the model parameters, as well as parameters of the algorithms used in the estimation process.
  * `MonteCarlo.h`      – header file containing the definitions and declarations of various functions, such as the ones used for finding a local minimum of objective function. In addition, the file contains some useful macro for reading approximation data from files.
  * `MonteCarlo.cpp`    – program performing the estimation of the measurement uncertainty using the Monte Carlo method.
  * `PoleZero.cpp`      – program computing the poles-zero pairs and coefficients for the polynomial approximations. Upon completion of the program, the calculated approximation parameters are stored in a number of files. The files containing pole-zero pairs and polynomial coefficients are saved into two separate folders.
  * `PoleZeroInit.cpp`  – performs intialization of poles-zero pair finding process. Upon completion of the program, the calculated approximation parameters are stored in a number of files prior to calculating the approximation coefficients by means of the `PoleZero.cpp` program.
  * `cylindrRT.cpp`     – function calculating of the reflection and transmission coefficients for a symmetrically placed cylindrical dielectric rod. The program uses the fast semi-analytical method developed by Sahalos and Vafiadis [3].
  * `MoMoff.cpp`        – function returning the reflection and transmission coefficients for an offset cylindrical dielectric rod. The program implements the method described in [4].

  * `librod.a`          – library containing compiled C++ functions for calculation of the reflection coefficient for a cylindrical dielectric rod, as well as functions for finding a minumum of single- and two- argument objective functions. 
  * `libslatec.a`       – the well-etablished SLATEC library containing FORTRAN subroutines and functions for calculation of various special mathematical functions including cylindrical functions (Bessel's functions) necessary for the methods desribed in [3,4].
  * `liblapack.a`       – the LAPACK library containing high-level routines for solving linear algebraic equation systems, as well as finding eigenvalues of matrices with real complex entries and performing various matrix decompositions.
  * `librefblas.a`      – the BLAS library of basic functions required for handling matrices and equation systems.
  * `libewald.a`        – library containing functions for evaluation of lattice-sums (Schlomilch series) arsing when treating an offset rod.

  * `runMonteCarlo.sh`  – script to run for the Monte Carlo estimation process.
  * `runPoleZero.sh`    – use this script to peform some intial calculations and preparation for the zero-pole pair finding process.
  * `runPoleZero.sh`    – run this script to calculate and save the zero-pole pairs after successful intialization.
  * `runPolyCoeff.sh`   – employ this script to evaluate the polynomial approximation coefficients necessary for a symmetrically placed rod.

  * `postprocessing.py` – Python script used for data post-processing.

  * `README.md`         – this readme file.
  * `README_fig`        – images included in this readme file.


# Model Assumptions 

* The dielectric cylindrical object under consideration is made of homogeneous material. 
* The height of the cylindrical rod equals that of the waveguide.
* For a symmetrically located cylindrical rod a mixed polynomial-rational approximation is utilized for the calculation of the S11 and S21 parameters that leads to a considerable speed-up in the estimation process.
* The function performing Monte Carlo estimation can be applied to other configurations, provided that the programs capable of computing the relevant scattering data, are available. For example, one can use the developed code with general-purpose solvers using the FEM, FDTD and MoM methods.

# References 

1. J. Krupka, “Microwave Measurements of Electromagnetic Properties of Materials,” Materials, vol. 14, no. 17. MDPI AG, p. 5097, Sep. 06, 2021. doi: 10.3390/ma14175097
2. JCGM 101 Supplement 1 to the GUM – Propagation of distributions using Monte Carlo method. (GUM-S1) 2008. Available:http://www.bipm.org/utils/common/documents/jcgm/JCGM_101_2008_E.pdf
3. Sahalos, J. N. and E. Vafiadis. On the narrow-band microwave Filter design using dielectric rod, IEEE Trans. Microwave Theory Technuques, Vol. 33, 1165-1171, 1985. 
4. R. Kushnin, J. Semenjako, and Y. V. Shestopalov, “Accelerated boundary integral method for solving the problem of scattering by multiple multilayered circular cylindrical posts in a rectangular waveguide,” 2017 Progress in Electromagnetics Research Symposium - Spring (PIERS), pp.3263–3271, January 2018. 
5. J. A. Nelder and R. Mead, “A Simplex Method for Function Minimization,” The Computer Journal, vol. 7, no. 4. Oxford University Press (OUP), pp. 308–313, Jan. 01, 1965. doi: 10.1093/comjnl/7.4.308.

# Software requirements

The demo case requires Linux environment, as well as the following software:
* Git - to obtain this demo case from GitLab by means of the clone command. 
* Python - needed to run pre- and post- processing Python scripts.
* OpenMPI - required for parallel processing. Versions 2.1.1 and 4.0.0 were tested.

# Obtaining the case

Download the demo model, use the following command to download all the necessary files
```
  git clone https://gitlab.com/Romans.Kusnins/dielectricconstantmeasuncertainty.git
```

# Running the demo

In case you are not using the RTU HPC, please, recompile the required libraries by first changing your working directory to `$GITPATH/MonteCarloEst` (replace `$GITPATH` with the path to the folder the `Git` repository is cloned into) and executing 
```
  make clean
```
so as to remove precompiled libraries, as well as the relevant objective files, and then compile the librarier from the scratch by issuing 
```
  make all
```
Once the libraries are prepared, you can either start the estimation process, or recalulate the parameters of the rational-polynomial approximation employed to considerably expedite the coputations. Note that the approximation is availbale for the symmetrically located rod only. 

In order to start the uncertainty estimation process, run the runMonteCarlo.sh script
```
  ./runMonteCarlo.sh
```
Upon completion of the script, the estimated mean value (best estimate of the measurand – dielectric constant) and the associated standard deviation are stored in the following file
```
  ESTDATA_ep_x_dr_y_dS11_z	- contains the calculated mean values and relative uncertainties
```
where x,y,z are the values of dielectric constant, uncertainty associated with the radius of the rod, and the one associated with the absolute value of the reflection coefficient (S11). The data from these two files can be exported and plotted using the following Python script:
```
  postproc.py
```
In case of a symmetrically placed rod a significant speed-up in the estimation process can be achieved by accelerating the calculation of the scattering data employing the polynomial-rational approximation. However, to use the approximation the relevant parameters, namely, pole-zero pairs (for constructing the rational part of the approximation) and coefficients of the approximating polynomials (for the polynomial part), must be calculated first. To do so, run the runPolyRat.sh script by executing
```
  ./runPolyCoeff.sh
```
Upon completion of the script, the calculated approximation coefficients are saved in the following two folders:`./poles_zeros` and `./poly_coeff`. Note that for the polynomial approximation the range of the values of the dielectric constant for which the approximation is to be found must be specified, as well as the approximating polynomial degree must be specified. In addition, the number of pole-zero pairs considered by the rational part of the approximation must be specified. 

# User modifications

The user can change the parameters of the structure under consideration, specify the parameters of minimum finding algorithm used to retrieve a dielectric constant from the measured data (synthetic data) and the number of trials for the Monte Carlo method analysis of the uncertainty. These parameters can be found in the `parameter.dat` file.

The parameters of the measurement model are listed below

- eps_r = 30.0
- tan_d = 0.001

- f  	= 10.0
- a  	= 22.86
- xo 	= 0.0

- BNUM = 11

where the parameter `eps_r` must be assigned the value of the dielectric constant of the rod (in case of a lossy dielectric `eps_r` corresponds to the real part of complex dielectric constant), `tan_d` is the loss tangent, `r` is the radius of the rod in mm, `f` is the operating frequency in GHz, `a` – the width of the broader wall of the waveguide and `xo` – is the offset in mm. The parameter `BNUM` corresponds to the number of basis functions used to approximate the fields inside the rod. More specifically, the greater the number of basis functions, the higher the accuracy of the calculated scattering data (S11 and S21). Note, that `BNUM` is used only when S11 and S21 are computed directly without any approximations (see the description of the parameter `APPROX` given below).

The parameters of the objective function minimizing algorithm are as follows

- ACC   = 0.0001
- LEN   = 0.01
- NUMIT = 1000

`ACC` is the convergence threshold for minimizing algorithm, the algorithm terminates when either the value of the objective function becomes less than `ACC`, or the maximum number of iterations (`NUMIT`) is reached. `LEN` is the size of initial search pattern.

The parameters of the Monte Carlo method are

- TRIALS = 1000000
- r_min 	= 1.0
- r_max 	= 10.0
- del_r 	= 0.005
- del_s11 = 0.02

where `TRIALS` indicates the number of trials – the number of times the model outcome is calculated, whereas the parameters `r_min` and `r_max` are intended to specify the lower and the upper limits of range of the rod radius values for which the uncertainty estimation is to be carried out. The other two parameters allow one to set the value of the measurement uncertaintie associated with radius of the rod (`del_r`) in mm, and the absolute value of the reflection coefficient (`del_S11`).

In case you desire to achieve an appreciable speed-in computation, enable the approximated calculations by specifying `yes` in the following line of the `parameters.dat` file

- APPROX = yes

By default the `APPROX` is set to `no` that assumes that the approximation is not used to calculate both S11 and S21.

Should you require to recalulate the polynomial approximation coefficients (the precalculated ones are for the range of `eps_r` from 40 to 90), change the lower and the upper limits of the desired range by specifying `r_min ` and `r_max `, respectively, in the same file. The degree of the approximating polynomials can be changed by assigning the parameter `ORD` a new value. 

# Numerical results

The figures given below show the Monte Carlo measurement uncertainty estimation results for a cylindrical dielectric rod symmetrically placed in a WR-90 rectangular waveguide. The width and height of the waveguide used in the computations were 22.86mm and 11.43mm, respectively. The frequency of the incident dominant waveguide mode was set to 10GHz. The estimated relative uncertainty and mean value are plotted as functions of the rod radius value in the range of 2mm to 10mm. The dielectric constant of the rod was assumed to be 40 and the number of Monte Carlo trials was 1000000. The estimation was performed for different values of the uncertainty associated with the absolute value of the reflection coefficient $`\mathrm{u}(|S|_{11})`$, whereas that associated with the radius of the rod was fixed and equal to $`\mathrm{u}(r)=3\mu m`$ 

In this example a rod located symmetrically with respect to the waveguide walls was considered only, though the developed code allows one to handle rods with non-zero offsets, as well.

<img src="./README_img/ESTDATA_40p00_dr_0003_dS11_0005_mean.png" alt="Figure 3a" width="400"/>

<img src="./README_img/ESTDATA_40p00_dr_0003_dS11_0005_uncer.png" alt="Figure 3b" width="400"/>

Figure 3: The mean value (left) and relative uncertainty (right) as functions of the rod radius, with $`\varepsilon_{\mathrm{r}}=40`$, $`\mathrm{u}(r)=3\mu m`$ and $`\mathrm{u}(|S|_{11})=0.005`$.

<img src="./README_img/ESTDATA_40p00_dr_0003_dS11_001_mean.png" alt="Figure 4a" width="400"/>

<img src="./README_img/ESTDATA_40p00_dr_0003_dS11_001_uncer.png" alt="Figure 4b" width="400"/>

Figure 4: The mean value (left) and relative uncertainty (right) as functions of the rod radius, with $`\varepsilon_{\mathrm{r}}=40`$, $`\mathrm{u}(r)=3\mu m`$ and $`\mathrm{u}(|S|_{11})=0.01`$.


<img src="./README_img/ESTDATA_40p00_dr_0003_dS11_002_mean.png" alt="Figure 5a" width="400"/>

<img src="./README_img/ESTDATA_40p00_dr_0003_dS11_002_uncer.png" alt="Figure 5b" width="400"/>

Figure 5: The mean value (left) and relative uncertainty (right) as functions of the rod radius, with $`\varepsilon_{\mathrm{r}}=40`$, $`\mathrm{u}(r)=3\mu m`$ and $`\mathrm{u}(|S|_{11})=0.02`$.

