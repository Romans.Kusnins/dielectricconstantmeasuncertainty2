#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<omp.h>

#include"PoleZero.h"


#ifdef MP
	#include<mpi.h>
#endif

//------------------------------------------------------------------------------------------------------------------

const char path[100] 		= "./poles_new_tr";
const char pathpoly[100] 	= "./poly_new_tr";

//------------------------------------------------------------------------------------------------------------------

int main(int argc,char * argv[]){

	int numprocs = 1.0, rank = 0;
	int namelen, provided;

	cdouble RT[2];

#ifdef MP

	char procname[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;
	MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 
	MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
	MPI_Get_processor_name(procname, &namelen);

#endif


//--------------------------------------------------------------------------------------

	NUM0 = 440;

	al = 0.75;

	GEN_FILE_POLES(al)

	printf("%s\n", poles_p);
	printf("%s\n", poles_n);
	
	POLES_OPEN(al)
	POLES_READ_R(al)
	POLES_CLOSE

	int alinc;

	double alo = al;

	if(rank < 4){
#ifndef MP
		for(int id=0; id<4; ++id){
#else
		for(int id=0; id<(numprocs>2 ? 1 : 2); ++id){	
#endif
			alinc = 0;

			for(int st=0; st<2001; ++st){
	
				r = NUM0*dr + 0.001; 

				if(rank % 2 == 0){
					al = alo - st*dal*0.001;
				}else{
					al = alo + st*dal*0.001;
				}

				kor = 2.0*PI*al*r;

//				printf("%5.12f\n", al);


			//-----------------------------------------------------------------------------

				besselj(0, kor, BJo, 2*M+1);
				bessely(0, kor, BYo, 2*M+1);

				besscyl_freq(al, PQPt, M, IP);

				double zri[2];

				if(rank > 1){
					for(int n=0;n<NPOLE;++n){	
						zri[0] = creal(lpo[n]);
						zri[1] = cimag(lpo[n]);
						nelder_mead(funmin_pos, zri, 0.01, 0.0000001, 400);
						lpo[n] = zri[0] + I*zri[1];
					}
				}else{
					for(int n=0;n<NPOLE;++n){	
						zri[0] = creal(lno[n]);
						zri[1] = cimag(lno[n]);
						nelder_mead(funmin_neg, zri, 0.01, 0.0000001, 400);
						lno[n] = zri[0] + I*zri[1];
					}
				}

				if(st % 1000 == 0 && st > 0){

					if(rank % 2 == 0){
						--alinc;
					}else{
						++alinc;
					}

					POLES_OPENW(alo + alinc*dal)

					if(rank > 1){
						POLES_WRITE_P(alo + alinc*dal)
					}else{
						POLES_WRITE_N(alo + alinc*dal)
					}

					POLES_CLOSE

					printf("STEP = %d\n", alinc);
					printf("rank = %d\n", rank);
				}
	
			}

		#ifndef MP		
			++rank;
		#else
			rank += 2;
		#endif
		}

	}

//------------------------------------------------------------------------------
//   RESULT PROCESSING SECTION
//------------------------------------------------------------------------------

#ifdef MP
	MPI_Finalize();
	printf("Process rank: %d\n\n", rank);
#endif

	return 0;

}

//====================================================================================

double funmin_pos(double * z){

	cdouble RT[2];
	cdouble zp = z[0]+I*z[1];

	if(z[0] > 2.0){
		return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'p')));
	}else{
     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
		return -cabs(RT[0]+RT[1]);
	}
}	

double funmin_neg(double * z){

	cdouble RT[2];
	cdouble zp = z[0]+I*z[1];

	if(z[0] > 2.0){
		return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'n')));
	}else{
     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
		return -cabs(RT[0]-RT[1]);
	}

}	




