//#include<complex>
#include<complex.h>
#include<math.h>
#include<stdio.h>

#define PI 3.1415926535897931

#define BJo(m)  BJo[m+N+1]
#define BHo(m)  BHo[m+N+1]
#define BJoD(m) BJoD[m+N]
#define BHoD(m) BHoD[m+N]

#define BJp(m)  BJp[m+N+1]
#define BHp(m)  BHp[m+N+1]
#define BJpD(m) BJpD[m+N]
#define BHpD(m) BHpD[m+N]

#define EHo(m)  EHo[m+N]
#define EX(m)   EX[m+N]

#define PH(m)   PH[m+N]
#define PHI(m)  PHI[m+N]

#define res_p(m)  res_p[2*N+m]
#define res_n(m)  res_n[2*N+m]

#define SM(m,n) SM[m+N+(n+N)*(2*N+1)]

#define cdouble _Complex double


//typedef std::complex<double> cdouble;
//cdouble I = std::complex<double>(0.0,1.0);


void besseljCPX(int, cdouble, cdouble *,int);
void besselyCPX(int, cdouble, cdouble *,int);


double expint(double x, int n, char mode);

void  exp_exp_int_ss(double * la, double x, double len, const int M, const int LAS, cdouble * res);
//void  exp_exp_int(double * la, double x, double y, double len, const int M, const int LAS, std::complex<double> * res);

extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);

void MoMoff(cdouble epo, double la, double ro, double xo, cdouble * RT, const int N){ //default value of N is 11

	const int L = 2*N+1;
	int nrhs = 1, info, pivot[4*N+2];
	int M = 2*N+1;

	double zo = 0.0;
	double Z = 120.0*PI;
	double kw, phk, k;

	cdouble ZO, ZOI;
	cdouble XO, XOI;
	cdouble BJo[2*N+3], BJp[2*N+3], BJoD[2*N+1], BJpD[2*N+1];
	cdouble BHo[2*N+3], BHp[2*N+3], BHoD[2*N+1], BHpD[2*N+1];
	cdouble SM[(2*N+1)*(2*N+1)];
	cdouble res_p[4*N+1], res_n[4*N+1];
	cdouble PH[2*N+1];
	cdouble PHI[2*N+1];
	cdouble EX[2*N+1];
	cdouble EHo[2*N+1];
	cdouble E11o, H11o;
	cdouble CF;

    //----------------------------------------------------------------------------

	epo = csqrt(epo);

	XO  = cexp(I*PI*xo);
	XOI = 1.0/XO;

	exp_exp_int_ss(&la, 0.0, 2.0, 2*N, 1, res_n);
	exp_exp_int_ss(&la, 2.0*xo, 2.0, 2*N, 1, res_p);

	k = 2.0*PI*la;

	besseljCPX(0, k*ro, &BJo[N+1], N+2);
	besseljCPX(0, epo*k*ro, &BJp[N+1], N+2);
	besselyCPX(0, k*ro, &BHo[N+1], N+2);
	besselyCPX(0, epo*k*ro, &BHp[N+1], N+2);

	for(int n=0;n<=N+1;++n){
		BHo(n) = BJo(n) + I*BHo(n);
		BHp(n) = BJp(n) + I*BHp(n);
	}
	for(int n=1;n<=N+1;++n){
		BJo(-n) = pow(-1.0,n)*BJo(n);
		BJp(-n) = pow(-1.0,n)*BJp(n);
		BHo(-n) = pow(-1.0,n)*BHo(n);
		BHp(-n) = pow(-1.0,n)*BHp(n);
	}
	for(int n=-N;n<=N;++n){
		BJoD(n) = 0.5*(BJo(n-1) - BJo(n+1));
		BJpD(n) = 0.5*(BJp(n-1) - BJp(n+1));

		BHoD(n) = 0.5*(BHo(n-1) - BHo(n+1));
		BHpD(n) = 0.5*(BHp(n-1) - BHp(n+1));
	}

	kw  = sqrt(k*k - PI*PI);
	phk = atan2(PI, kw);

	ZO  = cexp(I*kw*zo);
	ZOI = 1.0/ZO;

	for(int n=-N;n<=N;++n){
		PH(n)  = cexp(I*static_cast<double>(n)*phk);
		PHI(n) = 1.0/PH(n);

		EX(n) = -I*(cpow(I,n)*0.5*BJo(n)*ZO*(XO*PHI(n) - XOI*PH(n)));
		EHo(n) = BJpD(n)/BJp(n);
	}

	for(int m=-N;m<=N;++m){
		for(int n=-N;n<=N;++n){
			if(n%2==0){
				H11o = BJo(m)*(res_n(m-n) - res_p(n+m))*BJo(n);
				E11o = BJo(m)*(res_n(m-n) - res_p(n+m))*BJoD(n);
			}else{
				H11o = BJo(m)*(res_n(m-n) + res_p(n+m))*BJo(n);
				E11o = BJo(m)*(res_n(m-n) + res_p(n+m))*BJoD(n);
			}
			if(m==n){
				SM(m,n) = 1.0 + PI/2.0/I*ro*k*((E11o + BHo(m)*BJoD(m)) - epo*(H11o + BHo(m)*BJo(m))*EHo(n));
			}else{
				SM(m,n) = PI/2.0/I*ro*k*(E11o - epo*H11o*EHo(n));
			}
		}
	}


	zgesv_(&M, &nrhs, SM, &M, pivot, EX, &M, &info);

	RT[0] = 0.0;
	RT[1] = 1.0;

	for(int n=-N;n<=N;++n){

		CF = PI*k*ro*cpow(I,n)*(BJo(n)*epo*EHo(n) - BJoD(n))*EX(n)/kw;

		RT[0] -= (ZO*(XO*PH(n) - XOI*PHI(n)))*CF;
		if(n%2==0){
			RT[1] -= (ZOI*(XO*PHI(n) - XOI*PH(n)))*CF;
		}else{
			RT[1] += (ZOI*(XO*PHI(n) - XOI*PH(n)))*CF;
		}
	}

}


