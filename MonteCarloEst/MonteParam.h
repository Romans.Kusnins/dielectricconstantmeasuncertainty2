const double 	 eps_r 	 = 60.00000000;
const double 	 tan_d 	 = 0.00000000;

const bool 	 approx  = true;
const double 	 EPSMIN 	 = 40.00000000;
const double 	 EPSMAX 	 = 90.00000000;
const int 	 M  = 11;

const int 	 ORD  = 6;

const double 	 r_min 	 = 2.00000000;
const double 	 r_max 	 = 10.00000000;

const double 	 del_r 	 = 0.00500000;

const double 	 del_S11 	 = 0.02000000;

const double 	 f 	 = 10.00000000;
const double 	 a 	 = 22.86000000;
const double 	 x_o 	 = 0.00000000;

const double 	 ACC 	 = 0.00100000;
const double 	 LEN 	 = 0.01000000;
const int 	 NUMI 	 = 200;
const int 	 TRIALS  = 10000;
