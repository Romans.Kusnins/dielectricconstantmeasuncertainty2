#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<omp.h>
#include<string.h>
#include"MonteCarlo.h"

#ifdef MP
	#include<mpi.h>
#endif


int main(int argc, char * argv[]){


	cdouble		RT[2];
	cdouble		RTp1, RTn1;

	int numprocs = 1.0, rank = 0.0;
	int namelen, id, provided;

	int add_size, chunk0, chunk;
	int maxindit, instrand;


	FILE * fmean, *fsigma;

	double stepsize = 0.00025;

	int stepnum = static_cast<int>(floor((rmax - rmin)/stepsize));

//	printf("%d\n", stepnum);

	double * send;

	double * meanr_vec;
	double * sigmar_vec;
	double * S11real_vec;
	double * S11imag_vec;
	double * r_vec;

	double r_inc, al_inc;

	const long DISDIM = ceil(sqrt(TRIALS));

#ifdef MP

	char procname[MPI_MAX_PROCESSOR_NAME];

	MPI_Status status;
	MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(procname,&namelen);

	if(numprocs > stepnum){
		chunk    = 0;
		chunk0   = 1;
		add_size = stepnum;
	}else if(numprocs > 1){
		chunk    = floor(stepnum/numprocs);
		chunk0   = chunk + 1;
		add_size = stepnum % numprocs;
	}else{
		chunk    = 0;
		chunk0   = stepnum;
		add_size = 1;
	}
/*
	printf("stepnum = %d\n", stepnum);
	printf("chunk0 = %d\n", chunk0);
	printf("chunk = %d\n", chunk);
	printf("add_size = %d\n", add_size);

	printf("rmin = %5.12f\n", rmin);
	printf("rmax = %5.12f\n", rmax);
	printf("al = %5.12f\n", al);
*/

#else
	chunk    = 0;
	chunk0   = stepnum;
	add_size = 1;

#endif

	if(rank < add_size){
		send = new double[5*chunk0];
	}else{
		send = new double[5*chunk];
	}

	if(rank == 0){
		meanr_vec   	= new double[stepnum];
		sigmar_vec  	= new double[stepnum];
		S11real_vec 	= new double[stepnum];
		S11imag_vec 	= new double[stepnum];
		r_vec 		= new double[stepnum];
	}


	unsigned long int timet = ((unsigned long int) time(NULL));
	srand(1000000*time(NULL));


	FLPt fpoles_p, fpoles_n, fpolys_p, fpolys_n;
	FLPt fpolesf_p, fpolesf_n, fpolysf_p, fpolysf_n;

	flname polys_p, polys_n, poles_p, poles_n;
	flname polysf_p, polysf_n, polesf_p, polesf_n;


	double sigr1  = del_r/a;
	double sigal  = 0.000001;

	double sigR1  = del_S11;
	double sigT1  = 0.00;


	double meanr  = 0.0;
	double varr   = 0.0;
	double sigmar = 0.0;

	int R1I, FI;
	double err;


	R1I = 2*static_cast<int>(ceil(sigr1*3.0/dr));
	FI  = 2*static_cast<int>(ceil(sigal*6.0/dal));

	double ERR_R1[R1I][DISDIM];
	double ERR_F[FI][DISDIM];

	int it;
	int itr1 = 0;
	int ir1n = 0;

	double rest;
	double epstmp[2];

	int R1IN[R1I];
	int FIN[FI];


	for(int ri = 0; ri < ((rank < add_size) ? chunk0 : chunk); ++ri){


		if(rank >= stepnum){
			break;
		}

		if(rank < add_size){
			r1 = rmin + static_cast<double>((rank-1)*chunk0 + ri)*stepsize;
		}else{
			r1 = rmin + static_cast<double>(chunk0*add_size + (rank-add_size)*chunk + ri)*stepsize;
		}

		NUM01 = static_cast<int>(floor((r1-0.001)/dr));
		NUMR1 = NUM01 + 1;


		r_inc = r1/dr - (NUM01+1);

		alo = al;
		alf = alo + dal;


		for(it=0; it < R1I; ++it){
			R1IN[it] = 0;
		}
		for(it=0; it < FI; ++it){
			FIN[it] = 0;
		}
		it = 0;
		while(it < DISDIM){
			int tmp_id, IND;
			err = sigr1*RandN();
			tmp_id = static_cast<int>(floor(err/dr+r_inc));
			IND = tmp_id + R1I/2;
			if(IND>=0 && IND < R1I){
				ERR_R1[IND][R1IN[IND]] = err/dr + r_inc - tmp_id;
				++R1IN[IND];
				++it;
			}
		}
		it = 0;
		while(it < DISDIM){
			int tmp_id, IND;
			err = sigal*RandN()*RandN();
			tmp_id = static_cast<int>(floor(err/dal));
			IND = tmp_id + FI/2;
			if(IND>=0 && IND < FI){
				ERR_F[IND][FIN[IND]] = err/dal - tmp_id;
				++FIN[IND];
				++it;
			}
		}


		POLES_OPEN(alo);
		POLES_OPENF(alf);
		POLES_O(alo)
		POLES_F(alf)
		POLES_CLOSE
		POLES_CLOSEF

		POLY_OPEN(alo);
		POLY_OPENF(alf);
		POLY_O(alo)
		POLY_F(alf)
		POLY_CLOSE
		POLY_CLOSEF


		if(xo > 1.0E-12){
			MoMoff(eps, alo, r1, xo, RT, Nmom);
			RE1  = cabs(RT[0]);
			TE1  = cabs(RT[1]);
			REa1 = carg(RT[0]);
			TEa1 = carg(RT[1]);
		}else if(approx){
			RTn1 = 0.5*polynom(eps, r_inc, 0, rbnpt1, ORD)*fapp_2(eps, r_inc, 0, lnpt1, NE);
			RTp1 = 0.5*polynom(eps, r_inc, 0, rbppt1, ORD)*fapp_2(eps, r_inc, 0, lppt1, NE);

			RE1  = cabs(RTp1 + RTn1);
			TE1  = cabs(RTp1 - RTn1);
			REa1 = carg(RTp1 + RTn1);
			TEa1 = carg(RTp1 - RTn1);
		}else{
			kor = 2.0*PI*alo*(NUM01*dr+0.001);

			besselj(0, kor, BJo, 2*M+1);
			bessely(0, kor, BYo, 2*M+1);

			besscyl_freq(alo, PQPt, M, IP);

			cylindrRT(eps, NUM01*dr+0.001, alo, BJPt, PQPt, RT, M);

			RE1  = cabs(RT[0]);
			TE1  = cabs(RT[1]);
			REa1 = carg(RT[0]);
			TEa1 = carg(RT[1]);

		}

		NUM01E = NUM01;
		NUMR1E = NUMR1;

		aloe = alo;

		int smm=0;
		for(int itr=0; itr < FI; ++itr){
			smm += FIN[itr];
		}

		smm=0;
		for(int itr=0; itr < R1I; ++itr){
			smm += R1IN[itr];
		}

		meanr = 0.0;
		varr  = 0.0;

		for(int itf=0; itf < FI; ++itf){
			for(int ifn=0; ifn < FIN[itf]; ++ifn){

				alo = (itf - FI/2)*dal;
				fd  = ERR_F[itf][ifn];

				POLES_OPEN(aloe + alo);
				POLES_OPENF(aloe + alo + dal);
				POLY_OPEN(aloe + alo);
				POLY_OPENF(aloe + alo + dal);

				for(int it = 0; it < DISDIM; ++it){
					if(ir1n == R1IN[itr1]){
						++itr1;
						while(R1IN[itr1] == 0){
							++itr1;
						}
						ir1n = 0;
					}

        				NUM01 = itr1 - R1I/2 + NUM01E;
			        	rd1   = ERR_R1[itr1][ir1n];
				        NUMR1 = NUM01 + 1;

			        	POLES_O(alo);
				        POLES_F(alf);

				        POLY_O(alo);
				        POLY_F(alf);

				        epstmp[0] = EPSILO;   // specify the initial value used by the simplex method
				        epstmp[1] = cimag(eps);

			        	errR1 = sigR1*RandN();
				        errT1 = sigT1*RandN();

//				        nelder_mead(objfun, epstmp, 0.01, ACC, NUMI);
				        minseek(objfun1D, epstmp, 0.1, ACC, NUMI);

				        meanr += epstmp[0];
				        varr  += epstmp[0]*epstmp[0];

				        ++ir1n;

				}

				itr1=0;
				ir1n=0;

				POLES_CLOSEF
				POLES_CLOSE
				POLY_CLOSEF
				POLY_CLOSE

			}
		}

		meanr = meanr/DISDIM/DISDIM;
		sigmar = sqrt(varr/DISDIM/DISDIM - meanr*meanr);

	#ifdef MP

		if(rank != 0){
			send[0+5*ri]   = meanr;
			send[1+5*ri]   = sigmar;
			send[2+5*ri]   = RE1;
			send[3+5*ri]   = REa1;
			send[4+5*ri]   = r1;
		}else{
			meanr_vec[ri]   = meanr;
			sigmar_vec[ri]  = sigmar;
			S11real_vec[ri] = RE1;
			S11imag_vec[ri] = REa1;
			r_vec[ri] 	= r1;
		}

	#endif
	}


#ifdef MP

		if(rank != 0 && rank < add_size){
			MPI_Send(send, 5*chunk0, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
		}else if(rank != 0 && rank >= add_size && chunk !=0 && rank < stepnum){
			MPI_Send(send, 5*chunk, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
		}else if(rank == 0){
			for(int pid=1; pid<numprocs && pid<stepnum; ++pid){
				if(pid < add_size){
					MPI_Recv(send, 5*chunk0, MPI_DOUBLE, pid, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					for(int ri=0; ri<chunk0; ++ri){
						meanr_vec[ pid*chunk0 + ri]  	= send[0+5*ri];
						sigmar_vec[pid*chunk0 + ri]  	= send[1+5*ri];
						S11real_vec[pid*chunk0 + ri] 	= send[2+5*ri];
						S11imag_vec[pid*chunk0 + ri] 	= send[3+5*ri];
						r_vec[pid*chunk0 + ri]		= send[4+5*ri];
					}
				}else{
					MPI_Recv(send, 5*chunk, MPI_DOUBLE, pid, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					for(int ri=0; ri<chunk; ++ri){
						meanr_vec[ add_size*chunk0 + (pid-add_size)*chunk + ri] 	= send[0+5*ri];
						sigmar_vec[add_size*chunk0 + (pid-add_size)*chunk + ri] 	= send[1+5*ri];
						S11real_vec[add_size*chunk0 + (pid-add_size)*chunk + ri] 	= send[2+5*ri];
						S11imag_vec[add_size*chunk0 + (pid-add_size)*chunk + ri] 	= send[3+5*ri];
						r_vec[add_size*chunk0 + (pid-add_size)*chunk + ri] 		= send[4+5*ri];
					}
				}
			}


			filename(creal(eps), del_r, del_S11);

			fmean = fopen(meanflname,"w");
				fwrite(r_vec, 		sizeof(double), stepnum, fmean);
				fwrite(meanr_vec, 	sizeof(double), stepnum, fmean);
				fwrite(sigmar_vec, 	sizeof(double), stepnum, fmean);
				fwrite(S11real_vec, 	sizeof(double), stepnum, fmean);
				fwrite(S11imag_vec, 	sizeof(double), stepnum, fmean);
			fclose(fmean);

		}
/*
		if(rank == 0){
			for(int ri=0; ri<stepnum; ++ri){
				printf("mean = %5.12f\n", meanr_vec[ri]);
			}
			for(int ri=0; ri<stepnum; ++ri){
				printf("sigma = %5.12f\n", sigmar_vec[ri]);
			}
			for(int ri=0; ri<stepnum; ++ri){
				printf("S11 = %5.12f,%5.12f\n", S11real_vec[ri], S11imag_vec[ri]);
			}
			for(int ri=0; ri<stepnum; ++ri){
				printf("r = %5.12f\n", r_vec[ri]);
			}
			printf("\n");
		}
*/
	MPI_Finalize();
#endif

	return 0;

}
