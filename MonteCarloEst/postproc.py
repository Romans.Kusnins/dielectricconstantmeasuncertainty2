import numpy as np
from matplotlib import pyplot as plot

#------------------------------------------------------------

ep	= 40.0		# dielectric constant 
dr	= 0.003		# uncertainty in the rod radius
dS11	= 0.01		# uncertainty in |S11|

#------------------------------------------------------------
#  GENERATING THE FILE NAME
#------------------------------------------------------------

def dotrm(num, decpl, *opt):
	if len(opt) < 1:
		formt = "%1." + "%d" % (decpl) + "f"
		str = formt % (num)
		str = str.rstrip('0')
		id = str.find('.')
		str = str[0:id] + str[id+1:]
		return str
	else:
		formt = "%1." + "%d" % (decpl) + "f"
		str = formt % (num)
		str = str.replace('.', opt[0])
		return str

epstr	= dotrm(ep,2,'p')
drstr	= dotrm(dr,6)
dS11str = dotrm (dS11,6)

datstr  = "./DATA/ESTDATA_" + epstr +"_dr_" + drstr + "_dS11_" + dS11str + ".dat"
#------------------------------------------------------------------------

dat = np.fromfile(datstr, dtype=float)

LEN 	= int(len(dat)/5)

r 	= dat[0:LEN]
mnvl	= dat[LEN:2*LEN]
sig	= dat[2*LEN:3*LEN]


##plot.title("Standard uncertainty vs. rod radius")
##plot.xlabel("r, mm")
##plot.ylabel("u(\varpesilon_{\mathrm{r}}), %")

##plot.rcParams['text.usetex'] = True

fig1, ax1 = plot.subplots(figsize=(6,4), tight_layout=True)
ax1.plot(r, sig)
ax1.set_title("Relative uncertainty vs. rod radius")
ax1.set_xlabel(r'r, mm')
ax1.set_ylabel(r'Relative uncertainty, %')
ax1.grid()
plot.show()

fig2, ax2 = plot.subplots(figsize=(6,4), tight_layout=True)
ax2.plot(mnvl)
ax2.set_title("Mean value vs. rod radius")
ax2.set_xlabel(r'r, mm')
ax2.set_ylabel(r'Mean value')
ax2.grid()
plot.show()

##plot.plot(mnval)
##plot.show()

#print(meanstr)
#print(sigmastr)

