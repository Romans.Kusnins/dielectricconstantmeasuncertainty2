#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<omp.h>
#include<string.h>
#include"MonteParam.h"


#ifdef MP
	#include<mpi.h>
#endif

#define cdouble _Complex double 

const int SCD = sizeof(cdouble);

#define PI 3.141592653589793116


#define POW2(x) ((x)*(x))

//============================================================================================================

void 		besselj(int,cdouble,cdouble *,int);
void 		bessely(int,cdouble,cdouble *,int);
void 		besscyl_freq(double, cdouble * PQ[],const int, const int);
void 		besscyl_rad(cdouble eps, double r, double al, cdouble * BJ[],cdouble * gampt[], const int M);

void 		Legendre(int,double*,double*);
cdouble 	cylindr(cdouble, double, double, cdouble * BJPt[], cdouble * PQPt[],int,const char);
void  		cylindrRT(cdouble, double, double, cdouble * BJPt[], cdouble * PQPt[], cdouble *,int);

void 		polyfit(double,double,double,double,double,cdouble *BJPt[],cdouble *PQPt[],cdouble * lpt[],cdouble * rbpt[],const int,const int,const int, const int);

cdouble 	polynom(cdouble eps,double rd,double fd,cdouble * rbpt[], const int);
cdouble 	fapp_2(cdouble eps, double rd, double fd, cdouble * epspt[], const int);
//cdouble 	fapp(cdouble eps,cdouble * eps_o,const int);

void 		sortcr(cdouble *,const int);
void 		poles(double,double,cdouble,cdouble *,int,int,int,int,int,const char);
void 		nelder_mead(double (*) (double*),double *, double,double, const int);

void 		minseek(double (*)(double*), double *xo, double len, double acc, const int maxit);
void 		rmtrailzeros(char * str, double num);
void 		filename(double ep, double sigr1, double sigR1);

void 		MoMoff(cdouble, double, double, double, cdouble *, const int);
//============================================================================================================

cdouble eps = eps_r*(1.0 + I*tan_d);

const double EPSILO = creal(eps); 	// intial guess used to intialize the minimum seeking algorithm

const int  NE = 54;	    		// number of poles taken into account in the approximation of (R+T)(eps) and (R-T)(eps)
const int  M2 = M*M;	 		// number of basis (cylindrical) functions employed to approximate the fields

double al	 = round(a*f/3.0)*0.01;
double rmin	 = round(r_min/a*1000.0)*0.001;
double rmax	 = round(r_max/a*1000.0)*0.001;
double xo	 = round(x_o/a*1000.0)*0.001;

const int Nmom = 9;

double  dr   = 0.001;			// pole approximation radius step size 
double  dal  = 0.01;	 		// waveguide width to free-space wavelength ratio step size

cdouble kor;				// waveguide wavenumber times the rod radius

double r1;

const int IP = 21;


char path[100] 		= "./poles_zeros";	// path to the directory where pole database is located
char pathpoly[100] 	= "./poly_coeff"; 	// path to the directory where polynomial coefficient database is located

char sigflname[100];
char meanflname[100];

//=============================================================================================================

double rd1, fd;


double  errR1, errT1;

cdouble   P1[M2],P2[M2],Q1[M2],Q2[M2];
cdouble * PQPt[] = {P1,P2,Q1,Q2};

cdouble   BJo[2*M+1], BYo[2*M+1];
cdouble * BJPt[] = {BJo, BYo};

double    ph[19], ww[19];

cdouble   lpo1[NE], lno1[NE];
cdouble   lpr1[NE], lnr1[NE];
cdouble   lpf1[NE], lnf1[NE];
cdouble * lppt1[] = {lpo1,lpr1,lpf1};
cdouble * lnpt1[] = {lno1,lnr1,lnf1};

cdouble   rbpo1[ORD], rbno1[ORD];
cdouble   rbpr1[ORD], rbnr1[ORD];
cdouble   rbpf1[ORD], rbnf1[ORD];
cdouble * rbppt1[] = {rbpo1,rbpr1,rbpf1};
cdouble * rbnpt1[] = {rbno1,rbnr1,rbnf1};


double   RE1;
double   REa1;
double   TE1;
double   TEa1;

int NUM01, NUMR1; 
int NUM01E, NUMR1E; 

double alo, alf;
double aloe, alfe;


#define lpo1(i) lpo1[i-1]
#define lpr1(i) lpr1[i-1]
#define lpf1(i) lpf1[i-1]

#define lno1(i) lno1[i-1]
#define lnr1(i) lnr1[i-1]
#define lnf1(i) lnf1[i-1]

#define rbpo1(i) rbpo1[i-1]
#define rbpr1(i) rbpr1[i-1]
#define rbpf1(i) rbpf1[i-1]

#define rbno1(i) rbno1[i-1]
#define rbnr1(i) rbnr1[i-1]
#define rbnf1(i) rbnf1[i-1]


#define ph(i) ph[i-1]
#define ww(i) ww[i-1]

#define RandN() sqrt(-2.0*log(((double) rand())/RAND_MAX))*cos(2*PI*(((double) rand())/RAND_MAX))
#define RandU() (((double) rand())/RAND_MAX)

#define PRINT_POLY {for(m = 1; m <= ORD; ++m){printf("coef_p = (%5.15e,%5.15e) \n", rbp(m));}\
printf("\n");for(m = 1; m <= ORD; ++m){printf("coef_n = (%5.15e,%5.15e) \n", rbn(m));}}
#define PRINT_POLES(xp,xn) {for(numit=1; numit<=NE; ++numit){printf("eps = (%5.15f, %5.15f) \n", xp(numit));}\
printf("\n\n"); for(numit=1; numit<=NE; ++numit){printf("eps = (%5.15f, %5.15f) \n", xn(numit));}}

#define GEN_FILE_POLES(al) {sprintf(poles_p, "%s/poles_new_tr_%d_1_p", path, static_cast<int>((al)*100.0)); sprintf(poles_n, "%s/poles_new_tr_%d_1_n", path, static_cast<int>((al)*100.0));}
#define GEN_FILE_POLY(al)  {sprintf(polys_p, "%s/polys_new_tr_%d_1_p", pathpoly, static_cast<int>((al)*100.0)); sprintf(polys_n, "%s/polys_new_tr_%d_1_n", pathpoly, static_cast<int>((al)*100.0));}

#define GEN_FILE_POLESF(al) {sprintf(polesf_p, "%s/poles_new_tr_%d_1_p", path, static_cast<int>((al)*100.0)); sprintf(polesf_n, "%s/poles_new_tr_%d_1_n", path, static_cast<int>((al)*100.0));}
#define GEN_FILE_POLYF(al)  {sprintf(polysf_p, "%s/polys_new_tr_%d_1_p", pathpoly, static_cast<int>((al)*100.0)); sprintf(polysf_n, "%s/polys_new_tr_%d_1_n", pathpoly, static_cast<int>((al)*100.0));}

#define POLES_OPEN(al) {GEN_FILE_POLES(al); fpoles_p = fopen(poles_p,"r"); fpoles_n = fopen(poles_n,"r");}
#define POLY_OPEN(al)  {GEN_FILE_POLY(al);  fpolys_p  = fopen(polys_p, "r"); fpolys_n  = fopen(polys_n, "r");} 

#define POLES_CLOSE    {fclose(fpoles_p); fclose(fpoles_n);}
#define POLY_CLOSE     {fclose(fpolys_p); fclose(fpolys_n);}

#define POLES_OPENF(al) {GEN_FILE_POLESF(al); fpolesf_p = fopen(polesf_p,"r"); fpolesf_n = fopen(polesf_n,"r");}
#define POLY_OPENF(al)  {GEN_FILE_POLYF(al);  fpolysf_p  = fopen(polysf_p, "r"); fpolysf_n  = fopen(polysf_n, "r");} 

#define POLES_CLOSEF    {fclose(fpolesf_p); fclose(fpolesf_n);}
#define POLY_CLOSEF     {fclose(fpolysf_p); fclose(fpolysf_n);}


#define POLES_O(alo) {\
	fseek(fpoles_p,SCD*NUM01*NE,SEEK_SET);\
	fseek(fpoles_n,SCD*NUM01*NE,SEEK_SET);\
	fread(lpo1, SCD, NE, fpoles_p);\
	fread(lno1, SCD, NE, fpoles_n);\
	fseek(fpoles_p,SCD*NUMR1*NE,SEEK_SET);\
	fseek(fpoles_n,SCD*NUMR1*NE,SEEK_SET);\
	fread(lpr1, SCD, NE, fpoles_p);\
	fread(lnr1, SCD, NE, fpoles_n);}

#define POLES_F(alf) {\
	fseek(fpolesf_p,SCD*NUM01*NE,SEEK_SET);\
	fseek(fpolesf_n,SCD*NUM01*NE,SEEK_SET);\
	fread(lpf1, SCD, NE, fpolesf_p);\
	fread(lnf1, SCD, NE, fpolesf_n);}

#define POLY_O(alo) {\
	fseek(fpolys_p,SCD*NUM01*ORD,SEEK_SET);\
	fseek(fpolys_n,SCD*NUM01*ORD,SEEK_SET);\
	fread(rbpo1, SCD, ORD, fpolys_p);\
	fread(rbno1, SCD, ORD, fpolys_n);\
	fseek(fpolys_p,SCD*NUMR1*ORD,SEEK_SET);\
	fseek(fpolys_n,SCD*NUMR1*ORD,SEEK_SET);\
	fread(rbpr1, SCD, ORD, fpolys_p);\
	fread(rbnr1, SCD, ORD, fpolys_n);}


#define POLY_F(alf) {\
	fseek(fpolysf_p,SCD*NUM01*ORD,SEEK_SET);\
	fseek(fpolysf_n,SCD*NUM01*ORD,SEEK_SET);\
	fread(rbpf1, SCD, ORD, fpolysf_p);\
	fread(rbnf1, SCD, ORD, fpolysf_n);}


typedef char flname[100];
typedef FILE  *FLPt;

 
double objfun1D(double * deps){

	cdouble eps = deps[0] + I*deps[1];

	cdouble RT[2];
	cdouble RTp1, RTn1;

	double   R1, Ra1;
	double   T1, Ta1;

	double ra  = (NUM01 + rd1)*dr + 0.001;
	double ala =  aloe + alo + dal*fd;

//	printf("\n");
//	printf("%5.12f\n", r1);
//	printf("%5.12f\n", NUM01E*dr+0.001);
//	printf("%5.12f\n", ra);
//	printf("%5.12f\n", ala);
//	printf("%5.12f\n", aloe);
//	printf("%5.12f\n", rd1);
//	printf("%d\n", NUM01);
//	printf("%d\n", NUM01E);

//	printf("\n");
//	printf("%5.12f\n", R1);
//	printf("%5.12f\n", RE1);

	if(xo > 1.0E-12){
		MoMoff(eps, ala, ra, xo, RT, Nmom);

		R1  = cabs(RT[0]);
		T1  = cabs(RT[1]);
		Ra1 = carg(RT[0]);
		Ta1 = carg(RT[1]);
	}else if(approx){
		RTp1 = 0.5*polynom(eps, rd1, fd, rbppt1, ORD)*fapp_2(eps, rd1, fd, lppt1, NE);
		RTn1 = 0.5*polynom(eps, rd1, fd, rbnpt1, ORD)*fapp_2(eps, rd1, fd, lnpt1, NE); 

		R1  = cabs(RTp1 + RTn1); // absolute value of the reflection coefficient
		Ra1 = carg(RTp1 + RTn1); // phase of the reflection coefficient
	}else{
		kor = 2.0*PI*ala*ra;

                besselj(0, kor, BJo, 2*M+1);
                bessely(0, kor, BYo, 2*M+1);

                besscyl_freq(ala, PQPt, M, IP);

                cylindrRT(eps, ra, ala, BJPt, PQPt, RT, M);

		R1  = cabs(RT[0]);
		T1  = cabs(RT[1]);
		Ra1 = carg(RT[0]);
		Ta1 = carg(RT[1]);

//		printf("RE = %5.12f\n", RE1);
//		printf("RA = %5.12f\n", R1);
	}

	return POW2(R1 - RE1 - errR1);  // objective function

}


void filename(double ep, double sigr1, double sigR1){

	char epstr[40];
	char dr1str[40];
	char dS11str[40];
	
	char * stPt;

	sprintf(epstr, "%1.2f", ep);
	stPt = strstr(epstr,".");
	if(stPt != NULL) *stPt = 'p';

	rmtrailzeros(dr1str, sigr1);
	rmtrailzeros(dS11str, sigR1);

//	sprintf(sigflname,  "SIGMA_%s_dr_%s_dS11_%s.dat", epstr, dr1str, dS11str);
//	sprintf(meanflname, "MEANVAL_%s_dr_%s_dS11_%s.dat", epstr, dr1str, dS11str);
	sprintf(meanflname, "./DATA/ESTDATA_%s_dr_%s_dS11_%s.dat", epstr, dr1str, dS11str);

	printf("%s\n", sigflname);
	printf("%s\n", meanflname);

}


void rmtrailzeros(char * str, double num){

	char * stPt;
	int id = 0, last = -1;

	sprintf(str, "%1.8f", num);
	stPt = strstr(str,".");
	*stPt = '\0';				// by replacing '.' with '\0' one effectively splits it into two parts  
	sprintf(str, "%s%s", str, (stPt+1));

	stPt = str;
	while(*stPt != '\0'){
		if(*stPt != '0') last=id;
		++id;
		++stPt;
	}
	str[last+1] = '\0';
	sprintf(str,"%s", str);

}
