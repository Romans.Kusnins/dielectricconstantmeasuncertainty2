#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<omp.h>

#include"PoleZero.h"


#ifdef MP
	#include<mpi.h>
#endif

//------------------------------------------------------------------------------------------------------------------

const char path[100] 		= "./poles_new_tr/";
const char pathpoly[100] 	= "./poly_new_tr/";

//------------------------------------------------------------------------------------------------------------------

int main(int argc,char * argv[]){

	int numprocs=1.0, rank=0.0, namelen, provided;
	cdouble RT[2];

#ifdef MP

	char procname[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;
	MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 
	MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
	MPI_Get_processor_name(procname, &namelen);

	al = 0.80 - rank*0.01;
 
#endif


//--------------------------------------------------------------------------------------

NUM0 = 440;


#ifdef MP 
	int chunk;
	int chunk0;

	if(numprocs > 49){
		chunk  = 1;
		chunk0 = 1;
	}else if(numprocs > 1){
		chunk  = floor(49/(numprocs-1));
		chunk0 = 49 % (numprocs-1); 
	}else{
		chunk  = 0;  
		chunk0 = 49;		
	}
#else 
	const int chunk0 = 49; 
	const int chunk  = 0; 
#endif

//----------------------------------------------------------------------------

const int NUM_IT[] = {1401, 1401};
int sign;

for(int m=0; m<2; ++m){

	if(rank >= 49){
		break;
	}

	sign = (m==0) ? -1 : 1;

	for(int id=0; id<(rank==0 ? chunk0 : chunk); ++id){

		if(rank > 0){
			al = 0.51 + (chunk0 + chunk*(rank-1) + id)*dal;
		}else{
			al = 0.51 + id*dal;
		}

		POLES_OPEN(al)
		POLES_READ_R(al)
		POLES_CLOSE

		for(int st=0; st < NUM_IT[m]; ++st){
	
			r = NUM0*dr + 0.001 + sign*st*dr*0.01;

			kor = 2.0*PI*al*r;

			//-----------------------------------------------------------------------------

			besselj(0, kor, BJo, 2*M+1);
			bessely(0, kor, BYo, 2*M+1);

			besscyl_freq(al, PQPt, M, IP);

			double zri[2];

			for(int n=0;n<NPOLE;++n){	
				zri[0] = creal(lpo[n]);
				zri[1] = cimag(lpo[n]);
				nelder_mead(funmin_pos, zri, 0.01, 0.0000001, 400);
				lpo[n] = zri[0] + I*zri[1];
			}
			for(int n=0;n<NPOLE;++n){	
				zri[0] = creal(lno[n]);
				zri[1] = cimag(lno[n]);
				nelder_mead(funmin_neg, zri, 0.01, 0.0000001, 400);
				lno[n] = zri[0] + I*zri[1];
			}
			if(m == 0){
				if(st % 100 == 0 && st > 0){
					--NUM0; 
					POLES_OPENW(al)
					POLES_WRITE_P(al)
					POLES_WRITE_N(al)
					POLES_CLOSE
				}
			}else{
				if(st % 100 == 0 && st > 0){
					++NUM0; 				
					POLES_OPENW(al)
					POLES_WRITE_P(al)
					POLES_WRITE_N(al)
					POLES_CLOSE
				}
			}
//			printf("NUM0=%d\n", NUM0);

		}

	}

}

//------------------------------------------------------------------------------
// RESULT PROCESSING AND PREPARING SECTION
//------------------------------------------------------------------------------

#ifdef MP
	MPI_Finalize();
	printf("Process rank: %d\n\n", rank);
#endif

return 0;

}

double funmin_pos(double * z){

	cdouble RT[2];
	cdouble zp = z[0]+I*z[1];

	if(z[0] > 2.0){
		return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'p')));
	}else{
     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
		return -cabs(RT[0]+RT[1]);
	}

}	

double funmin_neg(double * z){

	cdouble RT[2];
	cdouble zp = z[0]+I*z[1];

	if(z[0] > 2.0){
		return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'n')));
	}else{
     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
		return -cabs(RT[0]-RT[1]);
	}
}	




